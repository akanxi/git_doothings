//
//  Cell.swift
//  Doothings
//
//  Created by AppDev03 on 3/3/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import Foundation
import UIKit
class lunchPlaceCell: UITableViewCell {
    @IBOutlet var lblHotelName: UILabel!
    @IBOutlet var lblStar: UILabel!
    @IBOutlet var lblDistanceinMile: UILabel!
    @IBOutlet var imgVenue: UIImageView!
    @IBOutlet var lblCategoryPrice: UILabel!
    
}

class VenueCell : UITableViewCell
{
    @IBOutlet weak var imgBig: UIImageView!
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var lblpricefor: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
}

class TrendingCell : UITableViewCell
{
    
    @IBOutlet var viewTrendingShadow: UIView!
    @IBOutlet var imgMain: UIImageView!
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var viewBottom: UIView!
    
    override func awakeFromNib() {
        imgMain.layer.borderWidth = 1.0
        imgMain.layer.borderColor = UIColor.init(colorLiteralRed: 44.0/255.0, green: 48.0/255.0, blue: 55.0/255.0, alpha: 0.30).cgColor
     viewTrendingShadow.dropShadow()
     viewBottom.layer.cornerRadius = 5.0
     viewTrendingShadow.layer.cornerRadius = 5.0
    }
}

class photoCell : UICollectionViewCell
{
    
    @IBOutlet var imgCollection: UIImageView!
}

class TipCell: UITableViewCell {
    @IBOutlet var imgUserProfile: UIImageView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblComment: UILabel!
    @IBOutlet var lblCreatedDate: UILabel!
    
}


class ActivityCell: UICollectionViewCell {
    @IBOutlet var imgActivity: UIImageView!
    
}

class CommunityCell: UICollectionViewCell {
    
    @IBOutlet var imgCommunity: UIImageView!
    
}

class HotelCell: UICollectionViewCell {
    
    @IBOutlet var lblHotelName: UILabel!
    @IBOutlet var imgTiger: UIImageView!
    @IBOutlet var lblAddress: UILabel!
    
}

