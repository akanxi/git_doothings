//
//  DetailViewController.swift
//  Doothings
//
//  Created by AppDev03 on 2/28/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import UIKit
import Alamofire

class DetailViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet var btnimgBigClose: UIButton!
    @IBOutlet var imgBig: UIImageView!
    @IBOutlet var imgMainVenue: UIImageView!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblOpenTime: UILabel!
    @IBOutlet var lblMenu: UILabel!
    @IBOutlet var lblVenueName: UILabel!
    @IBOutlet var collectionPhotos: UICollectionView!
    @IBOutlet var tblTip: UITableView!
    @IBOutlet var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet var lblAddressUpperDistance: NSLayoutConstraint!
    @IBOutlet var lblOpenTimeDistance: NSLayoutConstraint!
    @IBOutlet var lblMenuUpperDistance: NSLayoutConstraint!
    @IBOutlet var imgMenu: UIImageView!
    @IBOutlet var imgAddress: UIImageView!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblDescriptionUpperDisrtance: NSLayoutConstraint!
    
    @IBOutlet var lblmenuBottomHeight: NSLayoutConstraint!
    @IBOutlet var imgOpenTime: UIImageView!
    
    
    
    var VenueId : String = ""
    var venue : [String : Any] = [String : Any]()
    //    var venuePhotos : [AnyObject] = [AnyObject]()
    var venueTips : [AnyObject] = [AnyObject]()
    var contactNumber : String = ""
    var venueWebsiteURL : String = ""
    
    var venueData : VenueResponse?
    var venueDetail : Venue?
    var venuePhotos : [venuePhotoList] = [venuePhotoList]()
    var venueTipList : [TipDetail] = [TipDetail]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgBig.isHidden = true
        btnimgBigClose.isHidden = true
        getVenueData(venueId: VenueId)//Get Venue Data
        tblTip.isScrollEnabled = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Call
    @IBAction func btnCallPressed(_ sender: Any) {
        if let url = NSURL(string: "tel://\(contactNumber)"), UIApplication.shared.canOpenURL(url as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
                // Fallback on earlier versions
            }
            
        }
    }
    // Website
    @IBAction func btnWebsitePressed(_ sender: Any) {
        if (venueWebsiteURL.isEmpty == false)
        {
            let url = URL(string: venueWebsiteURL)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // Direction
    @IBAction func btnDirectionPressed(_ sender: Any) {
        let location = venueDetail?.vanueLocation
        
        if let lat = location?.lat {
            if let long = location?.lng{
                if (Sourcelatlong.isEmpty == false)
                {
                    let urlString: String = "http://maps.google.com/maps?daddr=\(lat),\(long)&saddr=\(Sourcelatlong)"
                    UIApplication.shared.openURL(URL(string: urlString)!)
                }
                
            }
            
        }
        
    }
    
    @IBAction func btnimgBigClosePressed(_ sender: Any) {
        btnimgBigClose.isHidden = true
        imgBig.isHidden = true
    }
    //MARK: - Get Venue Data
    func getVenueData(venueId : String) {
        let url : String = "https://api.foursquare.com/v2/venues/\(venueId)/"
        let param : [String : AnyObject] = [
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject
        ]
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding()).responseObject { (response:DataResponse<VenueResponse>) in
            self.venueData = response.result.value
            let responseGroup = self.venueData?.respo
            self.venueDetail = responseGroup?.venue
            
            self.UpdateUI()
        }
        
    }
    //set Api Data
    func UpdateUI() {
        //Set Best photp
        if let Photo = venueDetail?.bestPhoto
        {
            DispatchQueue.global(qos: .background).async {
                let url : String = "\(Photo.prefix!)300x300\(Photo.suffix!)"
                let urlOfImage = URL(string: url)
                DispatchQueue.main.async {
                    self.imgMainVenue.load.request(with: urlOfImage!)
                }
            }
            
        }
        
        lblVenueName.text = venueDetail?.name
        //Set ContactNumber
        if let contactNumberOf = venueDetail?.contact
        {
            if var cn = contactNumberOf.phone
            {
                contactNumber = cn
            }
        }
        //Set WebsiteURL
        
        if let siteURl = venueDetail?.url
        {
            venueWebsiteURL = siteURl
        }
        else
        {
            venueWebsiteURL = (venueDetail?.canonicalUrl)!
        }
        
        //Set Address
        if let venuelocation = venueDetail?.vanueLocation
        {
            lblAddress.text = venuelocation.address
            lblAddressUpperDistance.constant = 60.5
        }
        else
        {
            lblAddressUpperDistance.constant = 0
            imgAddress.isHidden = true
            lblAddress.isHidden = true
        }
        
        //Set Description
        if let description = venueDetail?.description
        {
            lblDescription.text = description
            lblDescriptionUpperDisrtance.constant = 49.5
        }
        else
        {
            lblDescription.text = ""
            lblDescriptionUpperDisrtance.constant = 0
        }
        //Open Hours
        //        if let hours = venue["hours"]
        //        {
        //
        //            lblOpenTime.text = "\((hours as AnyObject).value(forKey: "status")! as! String) Now"
        //            if let TimeFrames = (hours as AnyObject).value(forKey: "timeframes")
        //            {
        //                lblOpenTime.text = "\(lblOpenTime.text!) \(((TimeFrames as AnyObject).object(at: 0) as AnyObject).value(forKey: "days")!)"
        //                if let open  = ((TimeFrames as AnyObject).object(at: 0) as AnyObject).value(forKey: "open")
        //                {
        //                    lblOpenTime.text = "\(lblOpenTime.text!) \(((open as AnyObject).object(at: 0) as AnyObject).value(forKey: "renderedTime")!)"
        //                }
        //            }
        //            lblOpenTimeDistance.constant = 44.5
        //            lblOpenTime.isHidden = false
        //            imgOpenTime.isHidden = false
        //        }
        //        else
        //        {
        //            lblOpenTimeDistance.constant = 0
        //            lblOpenTime.isHidden = true
        //            imgOpenTime.isHidden = true
        //        }
        //Menu
        if let menu = venueDetail?.menu
        {
            lblMenu.text = menu.url
            lblMenuUpperDistance.constant = 43.5
            lblmenuBottomHeight.constant = 34.5
        }
        else
        {
            lblMenuUpperDistance.constant = 0
            lblMenu.isHidden = true
            imgMenu.isHidden = true
            lblmenuBottomHeight.constant = 25
        }
        // CollectionView Photos
        if ((venueDetail?.photo?.group?.count)! > 0)
        {
            if let VenueImages = venueDetail?.photo?.group?[0].photoItems
            {
                venuePhotos = VenueImages
                collectionPhotos.reloadData()
            }
        }
        
        
        //Reviews
        if let reviews = venueDetail?.tips?.tipgroup
        {
            venueTipList = reviews[0].tipitemlist!
            tblTip.reloadData()
        }
        
        
    }
    //MARK: - Close Pressed
    @IBAction func btnClosePressed(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    
    //MARK: - CollectionView Datasource,Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : photoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! photoCell
        cell.imgCollection.image = nil
        DispatchQueue.global(qos: .background).async {
            let url : String = "\(self.venuePhotos[indexPath.row].prefix!)300x300\(self.venuePhotos[indexPath.row].suffix!)"
            let urlOfImage = URL(string: url)
            DispatchQueue.main.async {
                cell.imgCollection.load.request(with: urlOfImage!)
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venuePhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let url : String = "\(venuePhotos[indexPath.row].prefix!)300x300\(venuePhotos[indexPath.row].suffix!)"
        let urlOfImage = URL(string: url)
        imgBig.load.request(with: urlOfImage!)
        imgBig.isHidden = false
        btnimgBigClose.isHidden = false
        
    }
    
    //MARK: - Tableview Datasource,Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TipCell = tableView.dequeueReusableCell(withIdentifier: "TipCell") as! TipCell
        if let comment = venueTipList[indexPath.row].text
        {
            cell.lblComment.text = comment
        }
        
        
        //Seconds to month
        let seonds : Float = Float( (venueTipList[indexPath.row].createdAt)!)
        let Hours = seonds / (60 * 60 * 24);
        cell.lblCreatedDate.text = (String(format: "%.f Hours Ago", Hours))
        if (Hours > 24)
        {
            
            let Days = seonds / (60 * 60 * 24 * 30);
            cell.lblCreatedDate.text = (String(format: "%.f days Ago", Days))
            if (Days > 30)
            {
                let Month = seonds / (60 * 60 * 24 * 30 * 12);
                if (Month > 0)
                {
                    cell.lblCreatedDate.text = (String(format: "%.f month Ago", Month))
                }
                
            }
        }
        if let user = venueTipList[indexPath.row].tipUser
        {
            cell.lblUserName.text = user.firstName
        }
        
        if let venueUser123 = venueTipList[indexPath.row].tipUser?.tipUserPhoto
        {
            cell.imgUserProfile.image = nil
            
            DispatchQueue.global(qos: .background).async {
                let url : String = "\(venueUser123.prefix!)300x300\(venueUser123.suffix!)"
                let urlOfImage = URL(string: url)
                DispatchQueue.main.async {
                    cell.imgUserProfile.load.request(with: urlOfImage!)
                    cell.imgUserProfile.layer.cornerRadius = cell.imgUserProfile.layer.frame.width/2
                    cell.imgUserProfile.clipsToBounds = true
                    
                }
            }
            
        }
        
        
        scrollViewHeight.constant = 371 + lblDescriptionUpperDisrtance.constant + lblDescription.frame.size.height + lblAddressUpperDistance.constant + lblAddress.frame.size.height +  lblOpenTimeDistance.constant + lblOpenTime.frame.size.height + lblMenuUpperDistance.constant + lblMenu.frame.size.height + lblmenuBottomHeight.constant + tblTip.contentSize.height + 210
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venueTipList.count
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
