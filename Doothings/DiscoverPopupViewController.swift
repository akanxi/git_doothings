//
//  DiscoverPopupViewController.swift
//  Doothings
//
//  Created by AppDev03 on 3/9/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import UIKit

class DiscoverPopupViewController: UIViewController {

    @IBOutlet var viewMain: UIView!
    
    
    @IBOutlet var btnDrinks: UIButton!
    @IBOutlet var btnFamily: UIButton!
    @IBOutlet var btnRestaurants: UIButton!
    @IBOutlet var btnOutdoors: UIButton!
    @IBOutlet var btnTrending: UIButton!
    @IBOutlet var btnDiscover: UIButton!
    
    @IBOutlet var lblTop: UILabel!
    
    var discoverBlock : ((_ discover : String)->())?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        btnDiscover.addTextSpacing(spacing: 2.5)
        btnFamily.addTextSpacing(spacing: 2.5)
        btnRestaurants.addTextSpacing(spacing: 2.5)
        btnOutdoors.addTextSpacing(spacing: 2.5)
        btnTrending.addTextSpacing(spacing: 2.5)
        btnDrinks.addTextSpacing(spacing: 2.5)
        lblTop.addTextSpacing(spacing: 0.5)
        viewMain.backgroundColor = UIColor.black.withAlphaComponent(0.90)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        viewMain = UIVisualEffectView(effect: blurEffect)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnPressed(_ sender: UIButton) {
        if (discoverBlock != nil)
        {
            discoverBlock!((sender.titleLabel?.text)!)
            dismiss(animated: false, completion: nil)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
