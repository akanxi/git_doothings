//
//  ViewController.swift
//  Doothings
//
//  Created by AppDev03 on 2/27/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper



class FirstViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CLLocationManagerDelegate {
    
    
    
    var priceTier    : String = ""
    var distanceFilter : Bool = Bool()
    var HotelArray   : [AnyObject] = [AnyObject]()
    let locationManager = CLLocationManager()
    var venueList : [GroupItems] = [GroupItems]()
    
    @IBOutlet var lblTitle: UILabel!
    var venueData : VenueResponse?
    
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var tblHotelList: UITableView!
    @IBOutlet var txtSearchPlace: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.lblTitle.text = Title_text
        navigationController?.isNavigationBarHidden = true
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
        }
        
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    //MARK: - Get User Current Location
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        Sourcelatlong = "\(locValue.latitude),\(locValue.longitude)"
        let latitide : String = (String(format: "%.2f", locValue.latitude))
        let logitude : String = (String(format: "%.2f", locValue.longitude))
        currentLocation = "\(latitide),\(logitude)"
        GetSearchRelatedData() //Get Searched Data
        
    }
    //MARK: - Fileter By Price
    @IBAction func btnPricepressed(_ sender: Any) {
        let controller : PricePopupViewController = storyboard?.instantiateViewController(withIdentifier: "PricePopupViewController") as! PricePopupViewController
        controller.modalPresentationStyle = UIModalPresentationStyle.custom
        controller.priceBlock = { (price) -> Void in
            if (price == "All")
            {
                self.priceTier = ""
            }
            else
            {
                self.priceTier = price
            }
            self.GetSearchRelatedData()
        }
        appdelegate.window?.rootViewController?.present(controller, animated: false, completion: nil)
    }
    //MARK: - Fileter By distance
    @IBAction func btnDistancePressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        distanceFilter = sender.isSelected
        GetSearchRelatedData()
    }
    
    //MARK: - Tableview Datasource Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : lunchPlaceCell = tableView.dequeueReusableCell(withIdentifier: "lunchPlaceCell") as! lunchPlaceCell
        cell.selectionStyle = .none
        //Name and Star
        let venueDetail  = venueList[indexPath.row].venue
        cell.lblHotelName.text = venueDetail?.name
        if let star = venueDetail?.rating
        {
            cell.lblStar.text = "\(venueDetail!.rating!) stars"
        }
       
        //Location
       if let venueLocation = venueDetail?.vanueLocation
       {
        if let totaldistace = venueLocation.distance
        {
            let distanceinMiles = totaldistace * 0.00062
            cell.lblDistanceinMile.text = (String(format: "%.1f Miles", distanceinMiles))

        }
        }
        
        if let venueCategory = venueDetail?.category
        {
            
            if (venueCategory.count > 0)
            {
                if let venuePrice = venueDetail?.price
                {
                    let tier = Int(venuePrice.tier!)
                    var currency : String = ""
                    for _ in 1...tier {
                        currency = "\(currency)\(venuePrice.currency!)"
                    }
                    
                    
                    cell.lblCategoryPrice.text = "\(cell.lblCategoryPrice.text!) \(currency)"
                }
            }
            
        }

        //venue photo
       cell.imgVenue.image = nil
        if let venuePhotos = venueDetail?.photo?.group
        {
            if (venuePhotos.count > 0)
            {
                if let venuePhotoslist = venuePhotos[0].photoItems
                {
                    
                    DispatchQueue.global(qos: .background).async {
                        let url : String = "\(venuePhotoslist[0].prefix!)300x300\(venuePhotoslist[0].suffix!)"
                        let urlOfImage = URL(string: url)
                        DispatchQueue.main.async {
                            cell.imgVenue.load.request(with: urlOfImage!)
                        }
                    }
                }
            }
            
        }

        
        return cell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venueList.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        let venue = venueList[indexPath.row].venue
        
        controller.VenueId = (venue?.id)!
        navigationController?.pushViewController(controller, animated: false)
    }
    
    //MARK: - Get Search Data
    func GetSearchRelatedData() {
        if (searchQuery.isEmpty == true){
            searchQuery = "Hotel"
        }
        
        let url : String = "\(BASE_URL)venues/explore/"
        
        var param : [String : AnyObject] = [
            "near" : nearByString as AnyObject,
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : searchQuery as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "sortByDistance":distanceFilter as AnyObject
        ]
        //Filter By Price
        if (priceTier.isEmpty == true)
        {
            param["price"] = [1,2,3,4] as AnyObject?
        }
        else
        {
            param["price"] = priceTier.characters.count as AnyObject
        }
        //Near by Location
        if (nearByString.isEmpty == true)
        {
            param["ll"] = "\(currentLocation)" as AnyObject?
        }
        else
        {
            param.removeValue(forKey: "ll")
        }
        
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseObject { (response:DataResponse<VenueResponse>) in
            self.venueData = response.result.value
            let responseGroup = self.venueData?.respo?.group
            self.venueList = (responseGroup?[0].iTems)!
            self.tblHotelList.reloadData()
            
           
        }
    }
    
    //MARK: - textField delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtLocation)
        {
            nearByString = textField.text!
        }
        if(textField == txtSearchPlace)
        {
            searchQuery = textField.text!
        }
        
        GetSearchRelatedData()
        self.view.endEditing(true)
        
        return false
    }
}




