//
//  HomeViewController.swift
//  Doothings
//
//  Created by AppDev03 on 3/6/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper



class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CLLocationManagerDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet var tblTrending: UITableView!
    @IBOutlet var trendingView: UIView!
    @IBOutlet var lblDiscover: UILabel!
    //MARK: - Title Labels
    @IBOutlet var btnSeeAll: UIButton!
    @IBOutlet var lblRestaurantsBottom: UILabel!
    @IBOutlet var lblActivities: UILabel!
    @IBOutlet var lblTours: UILabel!
    @IBOutlet var lblNightLife: UILabel!
    @IBOutlet var lblEvents: UILabel!
    @IBOutlet var lblCommunity: UILabel!
    @IBOutlet var lblHotels: UILabel!
    @IBOutlet var lblRestaurantsUp: UILabel!
    @IBOutlet var lblMovies: UILabel!
    @IBOutlet var btnDiscover: UIButton!
    
    @IBOutlet var btnWeekend: UIButton!
    @IBOutlet var btnThisweek: UIButton!
    @IBOutlet var btnToday: UIButton!
    @IBOutlet var btnAll: UIButton!
    @IBOutlet var viewShadow: UIView!
    @IBOutlet var AllView: UIView!
    //CollectionViews
    @IBOutlet var collectionHotel: UICollectionView!
    
    @IBOutlet var collectionCommunity: UICollectionView!
    
    @IBOutlet var collectionActivities: UICollectionView!
    //Constraints
    @IBOutlet var scrollviewContentHeight: NSLayoutConstraint! //Scrollview Height
    @IBOutlet var collectionHotelHeight: NSLayoutConstraint!
    @IBOutlet var viewSearchBox: UIView!
    @IBOutlet var HomeScrollView: UIScrollView!
    //Star
    @IBOutlet var imgStar1: UIImageView!
    
    @IBOutlet var imgStar2: UIImageView!
    
    @IBOutlet var imgStar3: UIImageView!
    
    @IBOutlet var imgStar4: UIImageView!
    
    @IBOutlet var imgStar5: UIImageView!
    @IBOutlet var imgStar2_1: UIImageView!
    @IBOutlet var imgStar2_2: UIImageView!
    @IBOutlet var imgStar2_3: UIImageView!
    @IBOutlet var imgStar2_4: UIImageView!
    @IBOutlet var imgStar2_5: UIImageView!
    
    
    @IBOutlet var imgStar3_1: UIImageView!
    @IBOutlet var imgStar3_2: UIImageView!
    @IBOutlet var imgStar3_3: UIImageView!
    @IBOutlet var imgStar3_4: UIImageView!
    @IBOutlet var imgStar3_5: UIImageView!
    
    var TvShowsData : [MovieResponse] = [MovieResponse]()
    
    var MovieData : MovieList?
    
    
    @IBOutlet var upView: UIView!
    
    let gradientLayer = CAGradientLayer()
    
    let locationManager = CLLocationManager()
    var venueList : [GroupItems] = [GroupItems]()
    
    var venueData : VenueResponse?
    var HotelDinnerArray   : [AnyObject] = [AnyObject]()
    var HotelLunchArray   : [AnyObject] = [AnyObject]()
    var HotelBreakfastArray   : [AnyObject] = [AnyObject]()
    var HotelArray   : [AnyObject] = [AnyObject]()
    var RestaurantSushiArray   : [AnyObject] = [AnyObject]()
    var RestaurantItalianArray   : [AnyObject] = [AnyObject]()
    var RestaurantFoodTruckArray   : [AnyObject] = [AnyObject]()
    var EventArray   : [AnyObject] = [AnyObject]()
    var NightLifeArray   : [AnyObject] = [AnyObject]()
    
    var tableReturn : Int = 0
    //Restaurants
    @IBOutlet var btnBreakfast: UIButton!
    @IBOutlet var btnDinner: UIButton!
    @IBOutlet var btnLunch: UIButton!
    @IBOutlet var imgDinner: UIImageView!
    @IBOutlet var imgBreakfast: UIImageView!
    @IBOutlet var imgLunch: UIImageView!
    @IBOutlet var btnDinnerSelect: UIButton!
    @IBOutlet var btnBreakfstSelect: UIButton!
    @IBOutlet var btnLunchSelect: UIButton!
    
    //Restaurants Bottom
    @IBOutlet var imgFoodTruck: UIImageView!
    @IBOutlet var imgItalian: UIImageView!
    @IBOutlet var imgShushi: UIImageView!
    
    //Movies
    @IBOutlet var btnMovies1: UIButton!
    @IBOutlet var btnMovies2: UIButton!
    @IBOutlet var btnMovies3: UIButton!
    
    
    @IBOutlet var lblMovie1Star: UILabel!
    //Events
    @IBOutlet var imgEvent1: UIImageView!
    @IBOutlet var imgEvent2: UIImageView!
    @IBOutlet var imgEvent3: UIImageView!
    @IBOutlet var lblEvent1Name: UILabel!
    @IBOutlet var lblEvent2Name: UILabel!
    @IBOutlet var lblEvent3Name: UILabel!
    
    //NightLife
    @IBOutlet var imgNightLife1: UIImageView!
    @IBOutlet var imgNightLife2: UIImageView!
    @IBOutlet var imgNightLife3: UIImageView!
    @IBOutlet var imgNightLife4: UIImageView!
    @IBOutlet var lblNightLife1: UILabel!
    @IBOutlet var lblNightLife2: UILabel!
    @IBOutlet var lblNightLife3: UILabel!
    @IBOutlet var lblNightLife4: UILabel!
    
    //MOvies
    @IBOutlet var imgMovie1: UIImageView!
    @IBOutlet var imgMovie2: UIImageView!
    @IBOutlet var imgMovie3: UIImageView!
    @IBOutlet var lblMovie2Name: UILabel!
    @IBOutlet var lblMovie3Name: UILabel!
    @IBOutlet var lblMovie1Name: UILabel!
    @IBOutlet var lblMovie1Category: UILabel!
    
    //TVShows
    @IBOutlet var lblTvshows: UILabel!
    @IBOutlet var btnTvShowsSeeAll: UIButton!
    @IBOutlet var imgTvShow1: UIImageView!
    @IBOutlet var imgTvShow2: UIImageView!
    @IBOutlet var imgTvShow3: UIImageView!
    @IBOutlet var lblTvshow1Name: UILabel!
    @IBOutlet var lblTvshow2Name: UILabel!
    @IBOutlet var lblTvshow3Name: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionCommunity.isScrollEnabled = false
        collectionHotel.isScrollEnabled = false
        collectionActivities.isScrollEnabled = false
        navigationController?.setNavigationBarHidden(true, animated: false)
        drawviewBorder(view: viewSearchBox, cornerRadis: 17.0, borderWidth: 1.0, borderColor: UIColor.clear)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (lblDiscover.text == "DISCOVER")
        {
            AllView.isHidden = false
            trendingView.isHidden = true
        }
        else
        {
            AllView.isHidden = true
            trendingView.isHidden = false
        }
        tblTrending.isScrollEnabled = false
        getMovieee()
        LineSpacing()
        
        viewShadow.dropShadow()
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
        }
        
    }
    
    //MARK: - Scrollview Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var pageNo: Float = Float((HomeScrollView.contentOffset.y / HomeScrollView.frame.size.height))
        pageNo.round()
        
        if (pageNo == 1.0)
        {
            if (lblDiscover.text == "DISCOVER")
            {
                if (HotelArray.count == 0) || (EventArray.count == 0)
                {
                    getHotelData()
                }
            }
            
            
        }
        if (pageNo == 2.0)
        {
            if (lblDiscover.text == "DISCOVER")
            {
                if (NightLifeArray.count == 0) || (RestaurantSushiArray.count == 0) || (RestaurantItalianArray.count == 0) || (RestaurantFoodTruckArray.count == 0)
                {
                    getNightLifeandRestaurants()
                }
            }

        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        var pageNo: Float = Float((HomeScrollView.contentOffset.y / HomeScrollView.frame.size.height))
        pageNo.round()
        
    }
    
    //MARK: - Seeall movie Pressed
    
    @IBAction func btnMovieSeeallpressed(_ sender: Any) {
        let controller : MovieListViewController = storyboard?.instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
        navigationController?.pushViewController(controller, animated: false)
    }
    //MARK: - Get User Current Location
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        Sourcelatlong = "\(locValue.latitude),\(locValue.longitude)"
        let latitide : String = (String(format: "%.2f", locValue.latitude))
        let logitude : String = (String(format: "%.2f", locValue.longitude))
        currentLocation = "\(latitide),\(logitude)"
//        GetCinepassMovieList()
        
        GetMovieRelatedData()
        getRestaurantData() //Get Restaurant Data
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnEventsAllPressed(_ sender: Any) {
        searchQuery = "Event"
        Title_text = "Event's"
        GotoList()
    }
    @IBAction func btnNightLifeAllPressed(_ sender: Any) {
        searchQuery = "nightlife"
        Title_text = "NightLife"
        GotoList()
    }
    
    @IBAction func btnRestaurantSeeAllPressed(_ sender: UIButton) {
        searchQuery = "Restaurant"
        Title_text = "Restaurant's"
        GotoList()
        
    }
    @IBAction func btnHotelsPressed(_ sender: Any) {
        searchQuery = "Hotel"
        Title_text = "Hotel's"
        GotoList()
        
    }
    @IBAction func btnNightLifeSeeAllPressed(_ sender: Any) {
        
        searchQuery = "Night Life"
        Title_text = "Night Life"
        GotoList()
    }
    
    //MARK: - Restaurant Pressed
    
    func GotoList() {
        let controller : FirstViewController = storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
        navigationController?.pushViewController(controller, animated: false)
    }
    @IBAction func btnDinnerPressed(_ sender: Any) {
        searchQuery = "Dinner"
        Title_text = "Dinner"
        GotoList()
    }
    @IBAction func btnBreakfastPressed(_ sender: Any) {
        searchQuery = "Breakfast"
        Title_text = "Breakfast"
        GotoList()
    }
    @IBAction func btnLunchPressed(_ sender: Any) {
        
        searchQuery = "Lunch"
        Title_text = "Lunch"
        GotoList()
    }
    @IBAction func btnFoodTrucksPressed(_ sender: Any) {
        searchQuery = "Food Trucks"
        Title_text = "Food trucks"
        GotoList()
    }
    @IBAction func btnItalianPressed(_ sender: Any) {
        searchQuery = "Italian"
        Title_text = "Italian"
        GotoList()
    }
    @IBAction func btnsushiPressed(_ sender: Any) {
        searchQuery = "sushi"
        Title_text = "sushi"
        GotoList()
    }
    
    //MARK: - EventPressed
    
    @IBAction func btnEvent1Pressed(_ sender: Any) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (EventArray[0].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
    }
    @IBAction func btnEvent2Pressed(_ sender: Any) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (EventArray[1].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
    }
    @IBAction func btnEvent3Pressed(_ sender: Any) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (EventArray[2].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
    }
    
    //MARK: - NightLife Pressed
    
    @IBAction func btnNightLife1Pressed(_ sender: Any) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (NightLifeArray[0].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
        
    }
    @IBAction func btnNightLife2Pressed(_ sender: Any) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (NightLifeArray[1].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
    }
    
    @IBAction func btnNightLife3Pressed(_ sender: Any) {
        
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (NightLifeArray[2].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
    }
    @IBAction func btnNightLife4Pressed(_ sender: Any) {
        let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        
        controller.VenueId = (NightLifeArray[3].value(forKey: "id")as? String)!
        navigationController?.pushViewController(controller, animated: false)
    }
    //MARK: - Movie Pressed
    
    @IBAction func btnMovie1Pressed(_ sender: Any) {
        if let moviedetail = self.MovieData?.movieDetail
        {
            let id : String = moviedetail[0].id! as String
            let controller : MovieDetailViewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
            controller.MovieId = id
            navigationController?.pushViewController(controller, animated: false)
            
            print(id)
        }
    }
    
    @IBAction func btnMovie2Pressed(_ sender: Any) {
        if let moviedetail = self.MovieData?.movieDetail
        {
            let id : String = moviedetail[1].id! as String
            let controller : MovieDetailViewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
            controller.MovieId = id
            navigationController?.pushViewController(controller, animated: false)
            print(id)
        }
    }
    
    @IBAction func btnMovie3Pressed(_ sender: Any) {
        if let moviedetail = self.MovieData?.movieDetail
        {
            let id : String = moviedetail[2].id! as String
            let controller : MovieDetailViewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
            controller.MovieId = id
            navigationController?.pushViewController(controller, animated: false)
            print(id)
        }
    }
    
    //MARK: - Top Dropdown Value Changed
    
    @IBAction func btnDIiscoverPressed(_ sender: Any) {

        let controller : DiscoverPopupViewController = storyboard?.instantiateViewController(withIdentifier: "DiscoverPopupViewController") as! DiscoverPopupViewController
        controller.discoverBlock = {(discoverString) -> Void in
           self.lblDiscover.text = discoverString
            self.lblDiscover.addTextSpacing(spacing: 2.5)
           
            if (self.lblDiscover.text == "DISCOVER")
            {
                self.collectionHotel.reloadData()
                self.collectionCommunity.reloadData()
                
                self.AllView.isHidden = false
                self.trendingView.isHidden = true
            }
            else
            {
                self.tableReturn = 5
                self.tblTrending.reloadData()
                self.AllView.isHidden = true
                self.trendingView.isHidden = false
            }
        }
        controller.modalPresentationStyle = .custom
        appdelegate.window?.rootViewController?.present(controller, animated: false, completion: nil)
    }
   //MARK: - tableview datasource and delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TrendingCell = tableView.dequeueReusableCell(withIdentifier: "TrendingCell") as! TrendingCell
        cell.selectionStyle = .none
        scrollviewContentHeight.constant = tblTrending.contentSize.height + 230
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableReturn
    }
    
    //MARK: - CollectionView Datasource delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == collectionHotel)
        {
            let cell : HotelCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HotelCell", for: indexPath) as! HotelCell
            cell.imgTiger.addGradiant(L1: 0.30, L2: 0.30, L3: 0.45, L4: 0.60)
            
            collectionHotelHeight.constant = collectionHotel.contentSize.height
            
            cell.lblHotelName.text = HotelArray[indexPath.row].value(forKey: "name")as? String
            
            if let location : [String : AnyObject] = (HotelArray[indexPath.row] as AnyObject).value(forKey: "location") as? [String : AnyObject]
            {
                
                if let address  = location["address"]
                {
                    
                    cell.lblAddress.text = (address) as? String
                    
                }
                
            }
            
            
            if let photos : [AnyObject] = ((self.HotelArray[indexPath.row].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
            {
                if (photos.count > 0)
                {
                    if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                    {
                        if (photos1.count > 0)
                        {
                            DispatchQueue.global(qos: .background).async {
                                let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                let urlOfImage = URL(string: url)
                                DispatchQueue.main.async {
                                    cell.imgTiger.load.request(with: urlOfImage!)
                                    cell.imgTiger.layer.cornerRadius = 5.0
                                    cell.imgTiger.clipsToBounds = true
                                }
                            }
                            
                            
                        }
                    }
                }
            }
            return cell
        }
        else if (collectionView == collectionCommunity)
        {
            let cell : CommunityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommunityCell", for: indexPath) as! CommunityCell
            return cell
        }
        else
        {
            let cell : ActivityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActivityCell", for: indexPath) as! ActivityCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return HotelArray.count
    }
//
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == collectionHotel)
        {
            let controller : DetailViewController = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
            
            controller.VenueId = (HotelArray[indexPath.row].value(forKey: "id")as? String)!
            navigationController?.pushViewController(controller, animated: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (lblDiscover.text == "DISCOVER")
        {
            if (ScreenWidth == 320) && (screenHeight == 480)
            {
                
                scrollviewContentHeight.constant = 4950 + 417
                return CGSize(width: CGFloat(142.0), height: CGFloat(125.0))
                
            }
            else if (ScreenWidth == 320) && (screenHeight == 568)
            {
                scrollviewContentHeight.constant = 4950 + 417
                return CGSize(width: CGFloat(142.0), height: CGFloat(125.0))
                
                
            }
            else if (ScreenWidth == 375) && (screenHeight == 667)
            {
                scrollviewContentHeight.constant = 4970 + 417
                return CGSize(width: CGFloat(169.5), height: CGFloat(135.0))
                
            }
            else if (ScreenWidth == 414) && (screenHeight == 736)
            {
                scrollviewContentHeight.constant = 5020 + 417
                return CGSize(width: CGFloat(189.0), height: CGFloat(160.0))
                
                
            }
            else
            {
                scrollviewContentHeight.constant = 5020 + 417
                return CGSize(width: CGFloat(189.0), height: CGFloat(160.0))
            }

        }
        else
        {
            
            return CGSize(width: CGFloat(189.0), height: CGFloat(160.0))
        }
        
    }
    
    
    func LineSpacing() {
        
        imgStar2_1.tintColor = UIColor.white
        imgStar2_2.tintColor = UIColor.white
        imgStar2_3.tintColor = UIColor.white
        imgStar2_4.tintColor = UIColor.white
        imgStar2_5.tintColor = UIColor.white
        
        imgStar1.tintColor = UIColor.white
        imgStar2.tintColor = UIColor.white
        imgStar3.tintColor = UIColor.white
        imgStar4.tintColor = UIColor.white
        imgStar5.tintColor = UIColor.white
        
        imgStar3_1.tintColor = UIColor.white
        imgStar3_2.tintColor = UIColor.white
        imgStar3_3.tintColor = UIColor.white
        imgStar3_4.tintColor = UIColor.white
        imgStar3_5.tintColor = UIColor.white
        
        imgDinner.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgLunch.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgBreakfast.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgMovie1.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgMovie2.addGradiant(L1: 0.0,L2: 0.60,L3: 0.85,L4: 1.0)
        imgMovie3.addGradiant(L1: 0.0,L2: 0.60,L3: 0.85,L4: 1.0)
        imgEvent1.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgEvent2.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgEvent3.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgNightLife1.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgNightLife2.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgNightLife3.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgNightLife4.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgFoodTruck.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgItalian.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        imgShushi.addGradiant(L1: 0.0,L2: 0.70,L3: 0.85,L4: 1.0)
        
        lblRestaurantsBottom.addTextSpacing(spacing: 0.5)
        lblActivities.addTextSpacing(spacing: 0.5)
        lblTours.addTextSpacing(spacing: 0.5)
        lblNightLife.addTextSpacing(spacing: 0.5)
        lblEvents.addTextSpacing(spacing: 0.5)
        lblCommunity.addTextSpacing(spacing: 0.5)
        lblHotels.addTextSpacing(spacing: 0.5)
        lblRestaurantsUp.addTextSpacing(spacing: 0.5)
        
        lblMovies.addTextSpacing(spacing: 0.5)
        btnSeeAll.addTextSpacing(spacing: 0.5)
        lblDiscover.addTextSpacing(spacing: 2.5)
        btnToday.addTextSpacing(spacing: 2.0)
        btnThisweek.addTextSpacing(spacing: 2.0)
        btnAll.addTextSpacing(spacing: 2.0)
        btnWeekend.addTextSpacing(spacing: 2.0)
    }
    
   }




