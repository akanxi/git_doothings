//
//  Models.swift
//  Doothings
//
//  Created by AppDev03 on 3/6/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class MovieResponse: Mappable {
    var Score: Int?
    var show : Show?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Score <- map["score"]
        show <- map["show"]
    }
}

class Show: Mappable {
    var id: String?
    var url: String?
    var name: String?
    var type: String?
    var language: String?
    var summary : String?
    var rating  : movieRating?
    var image   : MovieImage?
    var gener   : [movieGener]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        url <- map["url"]
        name <- map["name"]
        type <- map["type"]
        language <- map["language"]
        summary <- map["summary"]
        rating <- map["rating"]
        image <- map["image"]
        gener <- map["genres"]
    }
}

class movieRating  : Mappable {
    var average: Float?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        average <- map["average"]
        
    }
}

class movieGener  : Mappable {
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        
    }
}


class MovieImage  : Mappable {
    var medium: String?
    var original : String?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        medium <- map["medium"]
        
    }
}

class VenueResponse: Mappable {
    
    var meta: Meta?
    var respo: VenueExplore?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        meta <- map["meta"]
        respo <- map["response"]
    }
}

class Meta: Mappable {
    var code: String?
    var requestId: Int?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        requestId <- map["requestid"]
        
    }
}

class VenueExplore: Mappable {
    var headerLocation : String?
    var headerFullLocation : String?
    var query : String?
    var group :[Group]?
    var venue : Venue?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        group <- map["groups"]
        headerLocation <- map["headerLocation"]
        headerFullLocation <- map["headerFullLocation"]
        query <- map["query"]
        venue <- map["venue"]
    }
}

class Group: Mappable {
    var iTems : [GroupItems]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        iTems <- map["items"]
    }
}
class GroupItems: Mappable {
    var venue : Venue?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        venue <- map["venue"]
    }
}
class Venue: Mappable {
    var id : String?
    var name : String?
    var rating : Float?
    
    var vanueLocation : venueLocation?
    var category : [categories]?
    var price : Price?
    var photo : Photos?
    var bestPhoto : bestPhoto?
    var contact : contact?
    var tips : Tips?
    var canonicalUrl : String?
    var url : String?
    var menu : Menu?
    var description : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        rating <- map["rating"]
        vanueLocation <- map["location"]
        category <- map["categories"]
        price <- map["price"]
        photo <- map["photos"]
        bestPhoto <- map["bestPhoto"]
        contact <- map["contact"]
        tips <- map["tips"]
        canonicalUrl <- map["canonicalUrl"]
        url <- map["url"]
        menu <- map["menu"]
        description <- map["description"]
    }
}
class Menu: Mappable {
    var url : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        url <- map["url"]
        
    }
}

class Tips: Mappable {
    var tipgroup : [TipGroups]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        tipgroup <- map["groups"]
        
    }
}

class TipGroups: Mappable {
    var tipitemlist : [TipDetail]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        tipitemlist <- map["items"]
        
    }
}

class TipDetail: Mappable {
    var text : String?
    var tipUser : TipUser?
    var createdAt : NSNumber?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        text <- map["text"]
        tipUser <- map["user"]
        createdAt <- map["createdAt"]
    }
}

class TipUser: Mappable {
    var firstName : String?
    var lastName : String?
    var tipUserPhoto : tipUserPhoto?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        tipUserPhoto <- map["photo"]
    }
}

class tipUserPhoto: Mappable {
    var prefix : String?
    var suffix : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        prefix <- map["prefix"]
        suffix <- map["suffix"]
        
    }
}


class contact: Mappable {
    var phone : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        phone <- map["phone"]
        
    }
}


class bestPhoto: Mappable {
    var prefix : String?
    var suffix : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        prefix <- map["prefix"]
        suffix <- map["suffix"]
        
    }
}

class venueLocation: Mappable {
    var address : String?
    var distance : Float?
    var postalCode : String?
    
    var city : String?
    var state : String?
    var country : String?
    
    var lat : Float?
    var lng : Float?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        address <- map["address"]
        distance <- map["distance"]
        postalCode <- map["postalCode"]
        
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        
        lat <- map["lat"]
        lng <- map["lng"]
    }
}

class categories: Mappable {
    var name : String?
    var icon : categoryIcon?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        name <- map["name"]
        icon <- map["icon"]
        
    }
}

class categoryIcon: Mappable {
    var prefix : String?
    var suffix : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        prefix <- map["prefix"]
        suffix <- map["suffix"]
        
    }
}

class Price: Mappable {
    var tier : NSNumber?
    var currency : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        tier <- map["tier"]
        currency <- map["currency"]
        
    }
}

class Photos: Mappable {
    var group : [Photosgroups]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        group <- map["groups"]
        
    }
}

class Photosgroups: Mappable {
    var photoItems : [venuePhotoList]?
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        photoItems <- map["items"]
        
        
    }
}
class venuePhotoList: Mappable {
    var prefix : String?
    var suffix : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        prefix <- map["prefix"]
        suffix <- map["suffix"]
        
    }
}

class MovieList: Mappable {
    var movieDetail : [MovieDetail]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        movieDetail <- map["movies"]
        
    }
}

class MovieDetail: Mappable {
    var id : String?
    var slug : String?
    var title : String?
    var poster : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        slug <- map["slug"]
        title <- map["title"]
        poster <- map["poster_image_thumbnail"]
        
    }
}


class movieFullDetail: Mappable {
    var movie : movie?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        movie <- map["movie"]
        
    }
}

class movie: Mappable {
    
    var id : String?
    var title : String?
    var poster : String?
    var description : String?
    var trailor : [movieTrailor]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        poster <- map["poster_image"]
        description <- map["synopsis"]
        trailor <- map["trailers"]
    }
}
class movieTrailor: Mappable {
   
    var trailorFiles : [trailorFiles]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        trailorFiles <- map["trailer_files"]
        
    }
}

class trailorFiles: Mappable {
    
    var format : String?
    var url : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        format <- map["format"]
        url <- map["url"]
        
    }
}


class cinemaList: Mappable {
    var cinemas : [cinemas]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        cinemas <- map["cinemas"]
        
    }
}

class cinemas: Mappable {
    var id : String?
    var name : String?
    var slug : String?
    var telephone : String?
    var website : String?
    var booking_type : String?
    var location : Cinemalocation?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        slug <- map["slug"]
        telephone <- map["telephone"]
        website <- map["website"]
        booking_type <- map["booking_type"]
        location <- map["location"]
        
    }
}

class Cinemalocation: Mappable {
    var address : CinemaAddress?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        address <- map["address"]
       
    }
}

class CinemaAddress: Mappable {
    var address : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        address <- map["display_text"]
        
    }
}

class showTimeList: Mappable {
    var showtimes : [showtimes]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        showtimes <- map["showtimes"]
        
    }
}
class showtimes: Mappable {
    var id : String?
    var cinema_id : String?
    var movie_id : String?
    var start_at : String?
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        cinema_id <- map["cinema_id"]
        movie_id <- map["movie_id"]
        start_at <- map["start_at"]
        
    }
}
