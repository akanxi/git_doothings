//
//  MovieDetailViewController.swift
//  Doothings
//
//  Created by AppDev03 on 3/15/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
class showTimeCell: UICollectionViewCell {
    @IBOutlet var btnShowTime: UIButton!
    
}


class MovieDetailViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,ScrollableDatepickerDelegate {

    @IBOutlet var collectionShowTime: UICollectionView!
    @IBOutlet var imgMainPoster: UIImageView!
    @IBOutlet var lblMovieName: UILabel!
    @IBOutlet var lblOverView: UILabel!
    
    @IBOutlet var lblCinemaName: UILabel!
    @IBOutlet var lblCinemAddress: UILabel!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    
    var MovieDetail : movieFullDetail?
    var cinemaList : cinemaList?
    var cinemadata : [cinemas]?
    
    var ShowDetail : showTimeList?
    var showTimes : [showtimes] = [showtimes]()
    
    
    var cinemaId : String = ""
    var cinemaTitle : String = ""
    var cinemaAddress : String = ""
    var MovieId : String = ""
    
    @IBOutlet var youtubeView: YouTubePlayerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = getCurrentDate()
        let nextDate = getNextDate()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var datepicker: ScrollableDatepicker! {
        didSet {
            var dates = [Date]()
            for day in 0...60 {
                dates.append(Date(timeIntervalSinceNow: Double(day * 86400)))
            }
            
            datepicker.dates = dates
            datepicker.delegate = self
        }
    }
    
    func datepicker(_ datepicker: ScrollableDatepicker, didSelectDate date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM YYYY"
        print(formatter.string(from: date))
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetMovieDetail(id: MovieId)
        getCinemaNearBy()
//        getShowTime(id: MovieId)
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    func GetMovieDetail(id : String) {
        let url = "\(BASE_URL_CINEPASS)movies/\(id)"
        let param : [String : AnyObject] = [
            "apikey"         :CINEPASS_APIKEY as AnyObject,
            "fields"          :"id,title,poster_image.flat,synopsis,trailers" as AnyObject
        ]
        
//        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseJSON { (response:DataResponse<Any>) in
//        print(response.result.value)
//        }
        
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseObject { (response:DataResponse<movieFullDetail>) in
            self.MovieDetail = response.result.value
            if let movieData = self.MovieDetail?.movie
            {
                self.lblMovieName.text = movieData.title
                self.lblOverView.text = movieData.description
                self.lblOverView.sizeToFit()
                self.scrollHeight.constant = self.imgMainPoster.frame.size.height + 474 + self.lblOverView.frame.size.height
                DispatchQueue.global(qos: .background).async {
                    let url : String = movieData.poster!
                    let urlOfImage = URL(string: url)
                    DispatchQueue.main.async {
                        self.imgMainPoster.load.request(with: urlOfImage!)
                    }
                }
                if let movieTrailor = movieData.trailor?[0].trailorFiles?[0]
                {
                    if let movieURL = movieTrailor.url
                    {
                        let myVideoURL = NSURL(string: movieURL)
                        self.youtubeView.loadVideoURL(myVideoURL! as URL)
                        self.scrollHeight.constant = self.imgMainPoster.frame.size.height + 500 + self.lblOverView.frame.size.height
                    }
                }
                
                
            }
            
            
        }
        
    }
    
//    
//    func getShowTime(id : String) {
//        
//        let url = "\(BASE_URL_CINEPASS)showtimes/\(id)"
//        let param : [String : AnyObject] = [
//            "apikey"         :CINEPASS_APIKEY as AnyObject,
//            //            "fields"          :"id,title,poster_image.flat,synopsis,trailers" as AnyObject
//        ]
//        
//        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseJSON { (response:DataResponse<Any>) in
//            print(response.result.value)
//        }
//
//    }
    
    func getCinemaNearBy() {
        let url = "\(BASE_URL_CINEPASS)cinemas/"
        let param : [String : AnyObject] = [
            "apikey"         :CINEPASS_APIKEY as AnyObject,
            //            "fields"          :"id,title,poster_image.flat,synopsis,trailers" as AnyObject
            "movie_id"        : MovieId as AnyObject,
            "location"        : "32.91516,-117.117605" as AnyObject,
            "radius"          : 30 as AnyObject
        ]
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseObject { (response:DataResponse<cinemaList>) in
            self.cinemaList = response.result.value
            self.cinemadata = self.cinemaList?.cinemas
            if ((self.cinemadata?.count)! > 0)
            {
                self.cinemaId = (self.cinemadata?[0].id)!
                self.getShowtimeBycinema()
                self.lblCinemaName.text = self.cinemadata?[0].name
               if let cineaddress = self.cinemadata?[0].location?.address
                    {
                        self.cinemaAddress = cineaddress.address!
                        self.lblCinemAddress.text = self.cinemaAddress
                    }
                
            }
        }
        
    }
    
    func getShowtimeBycinema() {
        let nextDate = getNextDate()
        let url = "\(BASE_URL_CINEPASS)showtimes/"
        let param : [String : AnyObject] = [
            "apikey"         :CINEPASS_APIKEY as AnyObject,
            //            "fields"          :"id,title,poster_image.flat,synopsis,trailers" as AnyObject
            "movie_id"        : MovieId as AnyObject,
            "location"        : "32.91516,-117.117605" as AnyObject,
            "radius"          : 30 as AnyObject,
            "cinema_id"       : self.cinemaId as AnyObject,
            "time_to"         : nextDate as AnyObject
        ]
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseObject { (response:DataResponse<showTimeList>) in
            self.ShowDetail = response.result.value
            self.showTimes = (self.ShowDetail?.showtimes)!
            self.collectionShowTime.reloadData()
            
        }
//        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseJSON { (response:DataResponse<Any>) in
//            print(response.result.value)
//        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : showTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "showTimeCell", for: indexPath) as! showTimeCell
        let time = convertDateFormatter(date: (showTimes[indexPath.row].start_at)!)
        cell.btnShowTime.setTitle(time, for: .normal)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return showTimes.count
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
