//
//  PricePopupViewController.swift
//  Doothings
//
//  Created by AppDev03 on 3/2/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import UIKit
class PricePopupcell: UITableViewCell {
    @IBOutlet var lblCurrency: UILabel!
    
}

class PricePopupViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var priceBlock : ((_ Price : String)->())?

    @IBOutlet var priceTable: UITableView!
    var priceArray : [String] = ["$","$$","$$$","$$$$","All"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        priceTable.layer.cornerRadius = 2.0
        priceTable.layer.borderColor = UIColor.darkGray.cgColor
        priceTable.layer.borderWidth = 1.0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PricePopupcell = tableView.dequeueReusableCell(withIdentifier: "PricePopupcell") as! PricePopupcell
        cell.lblCurrency.text = priceArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return priceArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (priceBlock != nil)
        {
            priceBlock!(priceArray[indexPath.row])
            dismiss(animated: false, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
