//
//  utility.swift
//  Doothings
//
//  Created by AppDev03 on 2/27/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import Foundation
import Alamofire

public func WebServiceCaLL(url:String,method:HTTPMethod,parameter : NSDictionary,encoding : ParameterEncoding,completion : @escaping (_ Ret : NSDictionary)-> Void)
{
    Alamofire.request(url, method: method, parameters: parameter as? Parameters, encoding: encoding ).responseJSON { (response:DataResponse<Any>) in
        
        if (response.result.value != nil)
        {
            print(response.request)
            completion(response.result.value as! NSDictionary)
        }
        
        
    }
}

public func drawviewBorder(view : UIView,cornerRadis : Float,borderWidth : Float, borderColor : UIColor)
{
    view.layer.cornerRadius = CGFloat(cornerRadis)
    view.layer.borderWidth = CGFloat(borderWidth)
    view.layer.borderColor = borderColor.cgColor
}

public func getCurrentDate()-> String
{
    let date = Date()
    let Currentdate = date.iso8601
    return Currentdate
}
public func getNextDate()-> String
{
    let today = Date()
    let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: today)
    let nextDate = tomorrow?.iso8601
    return nextDate!
}
public func convertDateFormatter(date: String) -> String
{
    print(date)
    let dateFormatter = DateFormatter()
    
//    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"//this your string date format
    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
    let date = dateFormatter.date(from: date)
    print(date)
    
//    dateFormatter.dateFormat = "yyyy MMM EEEE HH:mm"///this is you want to convert format
    dateFormatter.dateFormat = "HH:mm a"
    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
    let timeStamp = dateFormatter.string(from: date!)
    
    
    return timeStamp
}


extension UIButton{
    func addTextSpacing(spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSRange(location: 0, length: (self.titleLabel?.text!.characters.count)!))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}


extension UILabel{
    func addTextSpacing(spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSRange(location: 0, length: self.text!.characters.count))
        self.attributedText = attributedString
    }
}
extension UIView {
    
    func dropShadow() {
        
        // corner radius
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    } }
extension UIImageView
{
    func addGradiant(L1:NSNumber,L2:NSNumber,L3:NSNumber,L4:NSNumber) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        
        let color1 = UIColor.clear.cgColor
        let color2 = UIColor.black.withAlphaComponent(0.50).cgColor
        let color3 = UIColor.black.withAlphaComponent(0.85).cgColor
        let color4 = UIColor.black.cgColor
        gradientLayer.colors = [color1, color2, color3, color4]
//        gradientLayer.locations = [0.0, 0.70, 0.85, 1.0]
        gradientLayer.locations = [L1, L2, L3, L4]
       
        self.layer.addSublayer(gradientLayer)
        self.clipsToBounds = true
        
    }
 
}
extension Date {
    static let iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
//        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss-xxx"
        return formatter
    }()
    var iso8601: String {
        return Date.iso8601Formatter.string(from: self)
    }
}

extension String {
    var dateFromISO8601: Date? {
        return Date.iso8601Formatter.date(from: self)
    }
}
