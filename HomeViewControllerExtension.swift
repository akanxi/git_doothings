//
//  HomeViewControllerExtension.swift
//  Doothings
//
//  Created by AppDev03 on 3/8/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
extension HomeViewController
{
    //MARK: - Get Hotel data
    func getHotelData() {
        let url : String = "\(BASE_URL)venues/explore/"
        
        let paramHotel : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Hotel" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "4" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        let paramEvents : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Event" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "4" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramHotel as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.HotelArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    self.collectionHotel.reloadData()
                    
                }
                
                
            })
            
        }
        
        
        
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramEvents as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.EventArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if (self.EventArray.count > 0)
                    {
                        
                        self.lblEvent1Name.text = self.EventArray[0].value(forKey: "name")as? String
                        self.lblEvent2Name.text = self.EventArray[1].value(forKey: "name")as? String
                        self.lblEvent3Name.text = self.EventArray[2].value(forKey: "name")as? String
                        if let photos : [AnyObject] = ((self.EventArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgEvent1.load.request(with: urlOfImage!)
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        if let photos : [AnyObject] = ((self.EventArray[1].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgEvent2.load.request(with: urlOfImage!)
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        
                        if let photos : [AnyObject] = ((self.EventArray[2].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgEvent3.load.request(with: urlOfImage!)
                                                
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        
                        
                    }
                }
                
                
            })
            
        }
        
        
        
        
        
        
    }
    
   //MARK: - Get NightLife
    
    func getNightLifeandRestaurants() {
        
        let url : String = "\(BASE_URL)venues/explore/"
        
        let paramSushi : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "sushi" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "2" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        
        let paramItalian : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Italian" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "2" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        
        let paramFoodTrucks : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Food trucks" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "2" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        
        
        
        let paramNightLife : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "nightlife" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "4" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramSushi as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.RestaurantSushiArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if let photos : [AnyObject] = ((self.RestaurantSushiArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                    {
                        if (photos.count > 0)
                        {
                            if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                            {
                                if (photos1.count > 0)
                                {
                                    DispatchQueue.global(qos: .background).async {
                                        let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                        let urlOfImage = URL(string: url)
                                        DispatchQueue.main.async {
                                            self.imgShushi.load.request(with: urlOfImage!)
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
                
                
            })
            
        }
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramItalian as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.RestaurantItalianArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if let photos : [AnyObject] = ((self.RestaurantItalianArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                    {
                        if (photos.count > 0)
                        {
                            if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                            {
                                if (photos1.count > 0)
                                {
                                    DispatchQueue.global(qos: .background).async {
                                        let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                        let urlOfImage = URL(string: url)
                                        DispatchQueue.main.async {
                                            self.imgItalian.load.request(with: urlOfImage!)
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
                
                
            })
            
        }
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramFoodTrucks as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.RestaurantFoodTruckArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if let photos : [AnyObject] = ((self.RestaurantFoodTruckArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                    {
                        if (photos.count > 0)
                        {
                            if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                            {
                                if (photos1.count > 0)
                                {
                                    DispatchQueue.global(qos: .background).async {
                                        let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                        let urlOfImage = URL(string: url)
                                        DispatchQueue.main.async {
                                            self.imgFoodTruck.load.request(with: urlOfImage!)
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
                
                
            })
            
        }
        
        
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramNightLife as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.NightLifeArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if (self.NightLifeArray.count > 0)
                    {
                        
                        
                        self.lblNightLife1.text = self.NightLifeArray[0].value(forKey: "name")as? String
                        self.lblNightLife2.text = self.NightLifeArray[1].value(forKey: "name")as? String
                        self.lblNightLife3.text = self.NightLifeArray[2].value(forKey: "name")as? String
                        self.lblNightLife4.text = self.NightLifeArray[3].value(forKey: "name")as? String
                        if let photos : [AnyObject] = ((self.NightLifeArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgNightLife1.load.request(with: urlOfImage!)
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        if let photos : [AnyObject] = ((self.NightLifeArray[1].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgNightLife2.load.request(with: urlOfImage!)
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        
                        if let photos : [AnyObject] = ((self.NightLifeArray[2].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgNightLife3.load.request(with: urlOfImage!)
                                                
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        if let photos : [AnyObject] = ((self.NightLifeArray[3].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                        {
                            if (photos.count > 0)
                            {
                                if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                                {
                                    if (photos1.count > 0)
                                    {
                                        DispatchQueue.global(qos: .background).async {
                                            let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                            let urlOfImage = URL(string: url)
                                            DispatchQueue.main.async {
                                                self.imgNightLife4.load.request(with: urlOfImage!)
                                                
                                            }
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        
                    }
                }
                
                
            })
            
        }
        
        
    }
    //MARK: - Get Restaurant Data
    func getRestaurantData() {
        let url : String = "\(BASE_URL)venues/explore/"
        
        let paramDinner : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Dinner" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "2" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        let paramLunch : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Lunch" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "2" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        let paramBreakfast : [String : AnyObject] = [
            
            "client_secret" : CLIENT_SECRET_KEY as AnyObject,
            "client_id" : CLIENT_ID as AnyObject,
            "v"         :"20140503" as AnyObject,
            "query"     : "Breakfast" as AnyObject,
            "venuePhotos" : "1" as AnyObject,
            "limit"     : "2" as AnyObject,
            "ll"        : ("\(currentLocation)" as AnyObject?)!
        ]
        
        
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramLunch as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.HotelLunchArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if let photos : [AnyObject] = ((self.HotelLunchArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                    {
                        if (photos.count > 0)
                        {
                            if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                            {
                                if (photos1.count > 0)
                                {
                                    DispatchQueue.global(qos: .background).async {
                                        let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                        let urlOfImage = URL(string: url)
                                        DispatchQueue.main.async {
                                            self.imgLunch.load.request(with: urlOfImage!)
                                            
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
                
                
            })
            
        }
        
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramBreakfast as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.HotelBreakfastArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if let photos : [AnyObject] = ((self.HotelBreakfastArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                    {
                        if (photos.count > 0)
                        {
                            if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                            {
                                if (photos1.count > 0)
                                {
                                    DispatchQueue.global(qos: .background).async {
                                        let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                        let urlOfImage = URL(string: url)
                                        DispatchQueue.main.async {
                                            self.imgBreakfast.load.request(with: urlOfImage!)
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
                
                
            })
            
        }
        
        DispatchQueue.global(qos: .background).async {
            
            WebServiceCaLL(url: url, method: .get, parameter: paramDinner as NSDictionary, encoding: URLEncoding() as ParameterEncoding, completion: {(SearchResponse) in
                
                let response : [String : Any] = SearchResponse.value(forKey: "response") as! [String : Any]
                if (response.count > 0)
                {
                    self.HotelDinnerArray = (((response["groups"]as AnyObject).object(at: 0) as AnyObject).value(forKey: "items")as AnyObject).value(forKey: "venue") as! [AnyObject]
                    if let photos : [AnyObject] = ((self.HotelDinnerArray[0].value(forKey: "photos")as AnyObject).value(forKey: "groups") as AnyObject).value(forKey: "items") as? [AnyObject]//Check if Photo Available
                    {
                        if (photos.count > 0)
                        {
                            if let photos1 : [AnyObject] = photos[0] as? [AnyObject]
                            {
                                if (photos1.count > 0)
                                {
                                    DispatchQueue.global(qos: .background).async {
                                        let url : String = "\(photos1[0].value(forKey: "prefix")!)300x300\(photos1[0].value(forKey: "suffix")!)"
                                        let urlOfImage = URL(string: url)
                                        DispatchQueue.main.async {
                                            self.imgDinner.load.request(with: urlOfImage!)
                                            
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
                
                
            })
            
        }
        
    }
    
    //MARK: = Get Tvshows detail
    func GetMovieRelatedData() {
      
        let url : String = "http://api.tvmaze.com/search/shows"
        
        let param : [String : AnyObject] = [
            
            "q"         :"girls" as AnyObject
            
        ]
       
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseArray { (response:DataResponse<[MovieResponse]>) in
            self.TvShowsData = response.result.value!
            self.lblMovie1Name.text = self.TvShowsData[0].show?.name
            self.lblMovie2Name.text = self.TvShowsData[1].show?.name
            self.lblMovie3Name.text = self.TvShowsData[2].show?.name

            if let rate : Float = self.TvShowsData[0].show?.rating?.average
            {
                var rateTodisplay = rate / 2
                rateTodisplay.round()
                let rates : Int = Int(rateTodisplay)
                print(rates)
                var imgArray : [UIImageView] = [self.imgStar1,self.imgStar2,self.imgStar3,self.imgStar4,self.imgStar5]
                
                for i in 0...rates - 1 {
                    let image = UIImage(named:"star")?.withRenderingMode(.alwaysTemplate)
                    imgArray[i].tintColor = UIColor.yellow
                    imgArray[i].image = image
                }

            }
            
            if let rate : Float = self.TvShowsData[1].show?.rating?.average
            {
                var rateTodisplay = rate / 2
                rateTodisplay.round()
                let rates : Int = Int(rateTodisplay)
                print(rates)
                var imgArray : [UIImageView] = [self.imgStar2_1,self.imgStar2_2,self.imgStar2_3,self.imgStar2_4,self.imgStar2_5]
                
                for i in 0...rates - 1 {
                    let image = UIImage(named:"star")?.withRenderingMode(.alwaysTemplate)
                    imgArray[i].tintColor = UIColor.yellow
                    imgArray[i].image = image
                }
                
            }

            if let rate : Float = self.TvShowsData[2].show?.rating?.average
            {
                var rateTodisplay = rate / 2
                rateTodisplay.round()
                let rates : Int = Int(rateTodisplay)
                print(rates)
                var imgArray : [UIImageView] = [self.imgStar3_1,self.imgStar3_2,self.imgStar3_3,self.imgStar3_4,self.imgStar3_5]
                
                for i in 0...rates - 1 {
                    let image = UIImage(named:"star")?.withRenderingMode(.alwaysTemplate)
                    imgArray[i].tintColor = UIColor.yellow
                    imgArray[i].image = image
                }
                
            }

            if let image = self.TvShowsData[0].show?.image
            {
                
                DispatchQueue.global(qos: .background).async {
                    let url : String = image.medium!
                    let urlOfImage = URL(string: url)
                    DispatchQueue.main.async {
                        self.imgMovie1.load.request(with: urlOfImage!)
                    }
                }
            }
            if let image = self.TvShowsData[1].show?.image
            {
                
                DispatchQueue.global(qos: .background).async {
                    let url : String = image.medium!
                    let urlOfImage = URL(string: url)
                    DispatchQueue.main.async {
                        self.imgMovie2.load.request(with: urlOfImage!)
                    }
                }
            }
            if let image = self.TvShowsData[2].show?.image
            {
                
                DispatchQueue.global(qos: .background).async {
                    let url : String = image.medium!
                    let urlOfImage = URL(string: url)
                    DispatchQueue.main.async {
                        self.imgMovie3.load.request(with: urlOfImage!)
                    }
                }
            }
        }
    }
    
    //MARK: - Get Movie Detail
    func getMovieee() {
        let url = "\(BASE_URL_CINEPASS)movies/"
        let param : [String : AnyObject] = [
            "lang"           :"de" as AnyObject,
            "apikey"         :CINEPASS_APIKEY as AnyObject,
            "limit"          : "4" as AnyObject
            
        ]
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseObject { (response:DataResponse<MovieList>) in
            self.MovieData = response.result.value
            if let moviedetail = self.MovieData?.movieDetail
            {
                DispatchQueue.global(qos: .background).async {
                    let url : String = moviedetail[0].poster! as String
                    let urlOfImage = URL(string: url)
                    
                    let url1 : String = moviedetail[1].poster! as String
                    let urlOfImage1 = URL(string: url1)
                    
                    let url2 : String = moviedetail[2].poster! as String
                    let urlOfImage2 = URL(string: url2)
                    
                    
                    if let movieTitle  = moviedetail[0].title! as? String
                    {
                        self.lblTvshow1Name.text = movieTitle as String
                    }
                    if let movieTitle  = moviedetail[1].title! as? String
                    {
                        self.lblTvshow2Name.text = movieTitle as String
                    }
                    if let movieTitle  = moviedetail[2].title! as? String
                    {
                        self.lblTvshow3Name.text = movieTitle as String
                    }
                    
                    DispatchQueue.main.async {
                        self.imgTvShow1.load.request(with: urlOfImage!)
                        self.imgTvShow2.load.request(with: urlOfImage1!)
                        self.imgTvShow3.load.request(with: urlOfImage2!)
                        
                    }
                }

            }
        }

    }
    

}
