//
//  MovieListViewController.swift
//  Doothings
//
//  Created by AppDev03 on 3/16/17.
//  Copyright © 2017 AppDev03. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
class MovieCell: UICollectionViewCell {
    
    @IBOutlet var viewBorder: UIView!
    @IBOutlet var imgMovie: UIImageView!
    @IBOutlet var lblMovieTitle: UILabel!
    @IBOutlet var lblMovieGener: UILabel!
}

class MovieListViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionMovieList: UICollectionView!
     var MovieData : MovieList?
     var movieDetail : [MovieDetail] = [MovieDetail]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        getMovieee()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MovieCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
        
        cell.viewBorder.layer.cornerRadius = 10.0
        cell.viewBorder.layer.borderWidth = 1.0
        cell.viewBorder.layer.borderColor = UIColor.init(colorLiteralRed: 216.0/255.0, green: 212.0/255.0, blue: 209.0/255.0, alpha: 1.0).cgColor
        cell.imgMovie.layer.cornerRadius = 10.0
        cell.imgMovie.layer.borderWidth = 1.0
        cell.imgMovie.layer.borderColor = UIColor.init(colorLiteralRed: 216.0/255.0, green: 212.0/255.0, blue: 209.0/255.0, alpha: 1.0).cgColor
        cell.imgMovie.clipsToBounds = true
        
        DispatchQueue.global(qos: .background).async {
            let url : String = self.movieDetail[indexPath.row].poster! as String
            let urlOfImage = URL(string: url)
            if let movieTitle  = self.movieDetail[indexPath.row].title
            {
                cell.lblMovieTitle.text = movieTitle as String
            }
            
            DispatchQueue.main.async {
                cell.imgMovie.load.request(with: urlOfImage!)
           }
        }
        
        
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieDetail.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let moviedetail = self.MovieData?.movieDetail
        {
            let id : String = moviedetail[indexPath.row].id! as String
            let controller : MovieDetailViewController = storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
            controller.MovieId = id
            navigationController?.pushViewController(controller, animated: false)
            
        }
    }
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    func getMovieee() {
        let url = "\(BASE_URL_CINEPASS)movies/"
        let param : [String : AnyObject] = [
            "lang"           :"de" as AnyObject,
            "apikey"         :CINEPASS_APIKEY as AnyObject,
            "limit"          : "20" as AnyObject
            
        ]
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding() as ParameterEncoding).responseObject { (response:DataResponse<MovieList>) in
            self.MovieData = response.result.value
            self.movieDetail = (self.MovieData?.movieDetail)!
            self.collectionMovieList.reloadData()
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (ScreenWidth == 320) && (screenHeight == 480)
        {
            
            return CGSize(width: CGFloat(147.0), height: CGFloat(250.0))
            
        }
        else if (ScreenWidth == 320) && (screenHeight == 568)
        {
            
            return CGSize(width: CGFloat(147.0), height: CGFloat(250.0))
            
            
        }
        else if (ScreenWidth == 375) && (screenHeight == 667)
        {
            
            return CGSize(width: CGFloat(169.5), height: CGFloat(135.0))
            
        }
        else if (ScreenWidth == 414) && (screenHeight == 736)
        {
            
            return CGSize(width: CGFloat(189.0), height: CGFloat(160.0))
            
            
        }
        else
        {
            
            return CGSize(width: CGFloat(189.0), height: CGFloat(160.0))
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
